package com.xb.chain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author xiaobin
 * @date 2020/3/5 21:41
 * @desc
 */
@SpringBootApplication
@EnableScheduling
public class ChainApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(ChainApplication.class);
    }

}
