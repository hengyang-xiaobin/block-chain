package com.xb.chain.controller;

import com.xb.chain.block.AbstractBlockChain;
import com.xb.chain.entity.Block;
import com.xb.chain.entity.Result;
import com.xb.chain.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author xiaobin
 * @date 2020/3/21 18:32
 * @desc
 */
@RestController
@RequestMapping("/block")
public class BlockController {


    @Autowired
    private AbstractBlockChain abstractBlockChain;


    @GetMapping("/mine")
    public Result mine() {
        List<Transaction> collect = Stream.of(
                abstractBlockChain.transfer("xiaobin", "zhangsan", new BigDecimal(20)), abstractBlockChain.transfer("xiaobin", "zhangsan", new BigDecimal(20))
        ).collect(Collectors.toList());
        Block block = abstractBlockChain.mineBlock(collect);
        return Result.ok("成功", block);
    }
}
