# block-chain
Java开发迷你区块链(目前只同步了节点区块，交易数据并没有同步)


chain-server:区块链服务端代码  
chain-server-boot:整合到springboot使用

### 项目目标    
1、了解区块链的结构  
2、学会创建一个区块(Block)  
3、学会创建区块链(BlockChain)  
4、学会向一个区块链上添加新的区块  
5、学会检验区块链路  
### 项目功能  
1、创世区块创建  
2、新增区块  
3、工作量证明  
4、P2P网络   
