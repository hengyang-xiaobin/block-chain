package com.xb.chain.utils;

import cn.hutool.json.JSONUtil;
import com.xb.chain.netty.message.entity.Message;

public class MessageProxyUtils {


    public static <T>  T converToBean(Object object,Class<T> clazz){
        Message message = (Message) object;
        return JSONUtil.toBean(JSONUtil.parseObj(message.getMessage()), clazz);
    }
}
