package com.xb.chain.constant;

public interface MessageType {

    public static final int STRING_MESSAGE = 1000;        // string类型消息

    public static final int NEW_CONNECTION = 1001;        // 新连接

    public static final int SYNC_BLOCK_INFO = 1002;       // 请求同步节点区块(首次)

    public static final int ACK_BLOCK_INFO = 1003;        // 回应同步节点区块

    public static final int GET_CHAIN_HEIGHT = 1004;      // 请求区块链的高度

    public static final int RETURN_CHAIN_HEIGHT = 1005;   // 返回区块链高度信息

}
