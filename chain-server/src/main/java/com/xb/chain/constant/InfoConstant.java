package com.xb.chain.constant;


import com.xb.chain.entity.ConnectionInfo;
import io.netty.channel.Channel;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author xiaobin
 * @date 2020/2/22 16:43
 * @desc
 */
public interface InfoConstant {

    public static final Integer THREAD_POOL_SIZE = 10;              // 线程池大小

    // 此服务器的Channel信息
    public static Map<ConnectionInfo, Channel> serverChannelGroup = new ConcurrentHashMap<>();

    // 连接其它服务器的客户端
    public static Map<ConnectionInfo, Channel> clientChannelGroup = new ConcurrentHashMap<>();


    static Channel getClientChannel(String ip, Integer port) {
        return mapGetClientChannel(ConnectionInfo.builder().ip(ip).port(port).build());
    }

    static Channel mapGetClientChannel(ConnectionInfo connectionInfo) {
        return InfoConstant.clientChannelGroup.get(connectionInfo);
    }
}
