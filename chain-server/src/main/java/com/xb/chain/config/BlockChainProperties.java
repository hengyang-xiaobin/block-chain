package com.xb.chain.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "xb.block.chain.node")
@Component
@Data
public class BlockChainProperties {


    private String serverIp;                    // 节点服务端启动IP

    private Integer port;                       // 节点服务端启动port

    private String dbUrl;                       // mysql url

    private String dbUsername;                  // mysql username

    private String dbPassword;                  // mysql password

}
