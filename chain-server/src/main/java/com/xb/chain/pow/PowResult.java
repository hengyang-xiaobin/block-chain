package com.xb.chain.pow;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author xiaobin
 * @date 2020/2/20 11:04
 * @desc
 */
@Data
@AllArgsConstructor
public class PowResult {

    private String hash;

    private Integer nonce;
}
