package com.xb.chain.netty.task;


import com.xb.chain.block.AbstractBlockChain;
import com.xb.chain.netty.message.handle.ClientToServerMessage;
import com.xb.chain.netty.server.NodeServerStart;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SyncNodeTask {


    /**
     *  定时同步节点信息
     */
    @Scheduled(initialDelay = 15000,fixedRate = 5000)
    public void syncNode(){
        log.info("block size:{}", AbstractBlockChain.list);
        log.info("sync node...");
        ClientToServerMessage.syncNodeHeightAll();
    }

}
