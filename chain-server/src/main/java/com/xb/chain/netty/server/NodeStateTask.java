package com.xb.chain.netty.server;

import lombok.extern.slf4j.Slf4j;

/**
 * @author xiaobin
 * @date 2020/2/28 19:33
 * @desc 循环监听服务端状态
 */
@Slf4j
public class NodeStateTask implements Runnable {
    @Override
    public void run() {
        int i = 0;
        for (; i == 0; ) {
            switch (NodeServerStart.nodeState) {
                case NODE_READY: {
                    break;
                }
                case NODE_REG: {
                    NodeServerStart.registerInfo();
                    break;
                }
                case NODE_NOTICE: {
                    NodeServerStart.noticeOtherNode();
                    break;
                }
                case NODE_SUCCESS: {
                    log.info("节点初始化完毕，关闭线程监听");
                    i = -1;
                    break;
                }
            }
        }
    }
}
