package com.xb.chain.netty.server;

/**
 * @author xiaobin
 * @date 2020/2/28 19:37
 * @desc 状态枚举
 */
public enum NodeState {

    NODE_READY,     // netty服务端就绪
    NODE_REG,       // netty服务端注册
    NODE_NOTICE,    // 通知各个节点服务新节点加入
    NODE_SUCCESS,   // 完成启动
}
