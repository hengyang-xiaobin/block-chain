package com.xb.chain.netty.message.entity;

import com.xb.chain.entity.Block;
import lombok.Builder;
import lombok.Data;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author xiaobin
 * @date 2020/2/26 20:40
 * @desc 同步节点信息
 */
@Builder
@Data
public class SyncNodeMessage{
    private String ip;                 // 需要同步节点端IP
    private Integer port;              // 需要同步节点端PORT
    private CopyOnWriteArrayList<Block> list;    // 同步的结点信息
}
