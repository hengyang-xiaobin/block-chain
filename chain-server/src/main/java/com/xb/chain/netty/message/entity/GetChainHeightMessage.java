package com.xb.chain.netty.message.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetChainHeightMessage{
    private String ip;                 // 需要同步节点端IP
    private Integer port;              // 需要同步节点端PORT

    private Integer height;     // 区块高度
}
