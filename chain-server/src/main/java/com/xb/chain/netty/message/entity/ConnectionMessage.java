package com.xb.chain.netty.message.entity;

import com.xb.chain.netty.client.NodeClientStart;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ConnectionMessage{
    private String ip;                 // 需要同步节点端IP
    private Integer port;              // 需要同步节点端PORT

    public void serverConnection(){
        NodeClientStart.serverConnection(this.ip,this.port);
    }
}
