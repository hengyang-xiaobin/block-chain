package com.xb.chain.netty.message.entity;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class Message<T> {

    private Integer messageType;        // 消息类型
    private T message;                  // 具体消息
    private Map<String,String> expand;  // 扩展字段

    private Message(){};

    private Message(Integer messageType,T message,Map<String,String> map){
        this.messageType = messageType;
        this.message = message;
        this.expand = map;
    };

    public static <T> Message<T> createMessage(Integer messageType,T message){
        return createMessage(messageType,message,new HashMap<>());
    }

    public static <T> Message<T> createMessage(Integer messageType,T message,Map<String,String> map){
        return new Message<T>(messageType,message,map);
    }

}
