package com.xb.chain.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author xiaobin
 * @date 2020/2/20 15:52
 * @desc
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transaction {

    public String from;
    public String to;
    public BigDecimal value;

}
