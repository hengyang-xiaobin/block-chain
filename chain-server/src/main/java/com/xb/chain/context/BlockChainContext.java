package com.xb.chain.context;

import com.xb.chain.netty.server.NodeServerStart;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * @author xiaobin
 * @date 2020/3/16 23:00
 * @desc
 */
@Component
@Slf4j
public class BlockChainContext implements ApplicationRunner {


    @Override
    public void run(ApplicationArguments args) throws Exception {
        // 启动节点服务器
        NodeServerStart.start();
    }
}
