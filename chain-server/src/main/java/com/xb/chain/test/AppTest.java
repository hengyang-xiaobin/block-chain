package com.xb.chain.test;

import com.xb.chain.block.AbstractBlockChain;
import com.xb.chain.block.BlockChain;
import com.xb.chain.transaction.Transaction;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author xiaobin
 * @date 2020/2/19 17:24
 * @desc
 */
@Slf4j
public class AppTest {

    public static void main(String[] args) {
        AbstractBlockChain blockChain = new BlockChain();
        blockChain.startBlock();

        List<Transaction> collect = Stream.of(
                blockChain.transfer("xiaobin", "zhangsan", new BigDecimal(20)), blockChain.transfer("xiaobin", "zhangsan", new BigDecimal(20))
        ).collect(Collectors.toList());

        blockChain.mineBlock(collect);
        blockChain.mineBlock(null);

        blockChain.currentBlockInfo();
        log.info("xiaobin:" + blockChain.getBalance("xiaobin").doubleValue());
        log.info("zhangsan:" + blockChain.getBalance("zhangsan").doubleValue());


    }
}
