package com.xb.chain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author xiaobin
 * @date 2020/2/23 22:12
 * @desc 连接信息
 */
@Data
@AllArgsConstructor
@Builder
public class ConnectionInfo {

    private String ip;
    private Integer port;

}
